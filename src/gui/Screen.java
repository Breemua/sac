package gui;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Screen extends JLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8610558473177399361L;
	
//	private JLabel screen;
	
	public Screen() {
		super();
//		this.screen = new JLabel();
//		this.setMinimumSize(new Dimension(500, 500));
		this.setPreferredSize(new Dimension(500, 500));
		
//		this.getViewport().add( screen );
		
	}
	
	public void display(BufferedImage image) {
		this.setIcon(new ImageIcon(image.getScaledInstance(this.getWidth(), this.getHeight(), Image.SCALE_DEFAULT)));
    }

}
