package gui;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class Window extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2448928286488870511L;
	
	private Screen screen;
	private AgentList agentList;

    public Window(int width, int height, map.Map map) {
    	super("Display");

        this.screen = new Screen();
    	this.agentList = new AgentList(map.getAllAgents());
        
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing( WindowEvent e) {
                System.exit(0);
            }
        });
        
        this.setSize(width, height);
        
   

        JPanel panel = new JPanel();
        this.getContentPane().add(panel);
//        panel.setLayout(new GridLayout(1,2));
        panel.setLayout(new BorderLayout());
        
        panel.add(this.screen, BorderLayout.CENTER);
        panel.add(this.agentList, BorderLayout.EAST);
        this.pack();
        this.setVisible(true);
    }

	public void display(BufferedImage image) {
		screen.display(image);
    }
	
	public void updateList() {
		agentList.updateList();
	}

	
}