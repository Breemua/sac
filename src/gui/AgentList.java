package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;

import agents.Agent;

public class AgentList extends JLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8475141236480580567L;

	private JList agentList;
	private List<Agent> agents;
	private JList infoList;
	private Agent selectedValue;
	
	public AgentList(List<Agent> agents) {
		super();
		this.agents = agents;
		this.selectedValue = null;
		
		this.setPreferredSize(new Dimension(200, 500));
		
		setLayout(new BorderLayout());
	    agentList = new JList(agents.toArray());
	    JScrollPane agentPane = new JScrollPane(agentList);
	    
	    infoList = new JList();
	    JScrollPane infoPane = new JScrollPane(infoList);
	    
	    add(agentPane, BorderLayout.NORTH);
	    add(infoPane, BorderLayout.CENTER);
	    
	    
	}

	public void updateList() {
		if(agentList.getSelectedValue() != null) {
			if(this.selectedValue != (Agent) agentList.getSelectedValue()) {
				this.selectedValue = (Agent) agentList.getSelectedValue();
			}
		}
		if(this.selectedValue != null) {
			infoList.setListData(this.selectedValue.getInfo().toArray());
		}
		agentList.setListData(agents.toArray());

	}
	
}
