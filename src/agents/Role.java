package agents;

import exceptions.NoPathException;
import general.Coordinate;
import general.Utils;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import map.Map;


public class Role {
	
	private int timeLeft;
	protected boolean toTownCenter;
	protected List<Coordinate> pathToFollow;

	
	public Role() {
		this.timeLeft = -42;
		this.toTownCenter = false; 
		this.pathToFollow = new ArrayList<Coordinate>();
	}
	
	public void run(Agent agent) {
		if(timeLeft == 0) {
			if(toTownCenter && pathToFollow.isEmpty()) {
//				agent.getCell().getBuilding().communicateWith((Human) agent);
				Map.townCenter.communicateWith((Human) agent);
				toTownCenter = false;
			} else {
				try {
					pathToFollow = //Agent.pathFinder.calcShortestPath(agent.coord, Map.townCenter.coord).getPath();
//						Utils.bresenham(agent.coord, Map.townCenter.coord);
							Utils.aStar(Agent.map, agent.coord, Map.townCenter.coord);
				} catch (NoPathException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				this.toTownCenter = true;
			}
		} else {
			timeLeft--;
		}
	}
	
	public void setMissionTime(int time) {
		this.timeLeft = time;
	}

	public void restart() {}

	public List<String> getInfo() {
		List<String> infos = new ArrayList<String>();
		infos.add("Mission time left: " + ((Integer) timeLeft).toString());
		return infos;
	}

	public Color getColor() {
		return new Color(0, 0, 0);
	}
	
}
