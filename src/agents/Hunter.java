package agents;

import exceptions.NoPathException;
import general.Coordinate;
import general.Utils;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import buildings.AnimalStable;
import buildings.Farm;

public class Hunter extends Role {
	
	private double currentDX;
	private double currentDY;
	private boolean hasDomesticableAnimal;
	private int meat;
	private AnimalStable animalStable;
	private Farm farm;
	
	private static final int visionRadius = 10;
	
	public Hunter() {
		super();
		this.currentDX = 1 - 2*Math.random();
		this.currentDY = 1 - 2*Math.random();
		this.hasDomesticableAnimal = false;
		this.meat = 0;
	}

	@Override
	public void run(final Agent agent) {
		super.run(agent);

		Comparator<Agent> c = new Comparator<Agent>() {
			@Override
			public int compare(Agent o1, Agent o2) {
				double d1 = o1.coord.getSquareDistanceFrom(agent.coord);
				double d2 = o2.coord.getSquareDistanceFrom(agent.coord);
				if(d1 < d2) {
					return -1;
				} else if(d1 == d2) {
					return 0;
				} else {
					return 1;
				}
			}
		};
		
		Collection<Agent> agentsInVisionRadius = agent.getAgentsAround(visionRadius);
		List<Animal> animalsInVisionRadius = new ArrayList<Animal>();
		for(Agent a : agentsInVisionRadius) {
			if(a instanceof Animal) {
				animalsInVisionRadius.add((Animal) a);
			}
		}
		Collections.sort(animalsInVisionRadius, c);
		
		if(this.meat > 0) {
			if(agent.coord.equals(this.farm.coord)) {
				this.meat = this.farm.fill(this.meat);
			}
		}
		
		if(this.hasDomesticableAnimal) {
			if(agent.coord.equals(this.animalStable.coord)) {
				this.animalStable.addAnimal();
				this.hasDomesticableAnimal = false;
			}
		}
		
		if(this.pathToFollow.isEmpty()) {
			if(this.meat > 0) {
				try {
					this.pathToFollow = Utils.aStar(Agent.map, agent.coord, this.farm.coord);
				} catch (NoPathException e) {
					e.printStackTrace();
				}
			} else if(this.hasDomesticableAnimal) {
				try {
					this.pathToFollow = Utils.aStar(Agent.map, agent.coord, this.animalStable.coord);
				} catch (NoPathException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		if(this.pathToFollow.isEmpty()) {
			if(animalsInVisionRadius.isEmpty()) {
				this.pathToFollow.add(this.moveRandomly(agent));
			} else {
				Animal a = animalsInVisionRadius.get(0); 
				if(a.coord.equals(agent.coord)) {
					int result = a.encounters(agent);
					if(result > 0) {
						meat = result;
					} else if (result == 0){
						this.hasDomesticableAnimal = true;
					} else {
						agent.die();
						System.out.println("Agent dead");
					}
				}
				this.pathToFollow.add(this.modeRandomlyTo(agent, a));
			}
		}
		
		((MovingAgent) agent).move(pathToFollow.get(0));
		this.pathToFollow = pathToFollow.subList(1, pathToFollow.size());
		
	}
	
	private Coordinate modeRandomlyTo(Agent agent, Animal animal) {
		return Utils.moveRandomTo(agent, -agent.coord.x + animal.coord.x, 
					 -agent.coord.y + animal.coord.y, 5, 1);		
	}

	private Coordinate moveRandomly(Agent agent) {
		Coordinate to = Utils.moveRandomTo(agent, currentDX, currentDY, 3, 1);
		while(!Agent.map.isVisitable(to)) {
			this.currentDX = 1 - 2*Math.random();
			this.currentDY = 1 - 2*Math.random();
			to = Utils.moveRandomTo(agent, currentDX, currentDY, 3, 1);
		}
		return to;
	}
	
	
	
	
	/**
	 * @return the animalStable
	 */
	public AnimalStable getAnimalStable() {
		return animalStable;
	}

	/**
	 * @param animalStable the animalStable to set
	 */
	public void setAnimalStable(AnimalStable animalStable) {
		this.animalStable = animalStable;
	}

	/**
	 * @return the foodShelter
	 */
	public Farm getFarm() {
		return farm;
	}

	/**
	 * @param setFarm the foodShelter to set
	 */
	public void setFarm(Farm farm) {
		this.farm = farm;
	}

	@Override
	public List<String> getInfo() {
		List<String> current = super.getInfo();
		current.add("Has domesticable animal: " + hasDomesticableAnimal);
		current.add("Meat: " + meat);
		return current;
	}

	@Override
	public Color getColor() {
		return new Color(255, 0, 0);
	}
	

}
