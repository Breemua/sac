package agents;

import exceptions.NoPathException;
import exceptions.NonConstructibleException;
import general.Coordinate;
import general.Utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import map.Cell;
import map.Content;
import map.Topography;
import buildings.Farm;
import buildings.Field;

public class Farmer extends Role{
	
	/**
	 * variables
	 */
	int vegetables;
	int goingBackToCity;
	private int timeToGrowOnField;
	private static final int visionRadius 	= 15;
	private static final int timeToGrownOnFieldInit = 35;
	
	private double currentDX;
	private double currentDY;
	
	List<Coordinate> pathToFollowPlain;
	List<Coordinate> pathToFollowBackToCenter;
	List<Coordinate> pathToFollowField;
	
	boolean foundPlain = false;
	
	private Field field;
	
	private Farm farm;
	
	/**
	 * Constructor
	 */
	
	public Farmer(){
		
		super();
		
		this.vegetables 				= 0;
		this.pathToFollowBackToCenter 	= new ArrayList<Coordinate>();
		this.pathToFollowPlain 			= new ArrayList<Coordinate>();
		this.pathToFollowField 			= new ArrayList<Coordinate>();
		
		this.currentDX 					= 1 - 2*Math.random();
		this.currentDY 					= 1 - 2*Math.random();
		
		this.goingBackToCity = 0;
		this.timeToGrowOnField = Farmer.timeToGrownOnFieldInit;
		
		
	}
	
	/**
	 *
	 * Search for plain or free field 
	 * 
	 * 		if plain => change to content field type building
	 * 		work it (timeToGrowOnField) and go back to foodShelter
	 * 
	 * 		if field => if(!capacity full)
	 * 			no - we go back searching
	 * 			yes - work it and go back to foodShelter
	 * 
	 * 
	 */
	

	@Override
	public void run(Agent agent) {
		super.run(agent);

		
		
		/**
		 * Go explore
		 * find plain or find free field
		 */

		if(foundPlain == false && goingBackToCity == 0)
		{
			//vegetables = 0;
			foundPlain = findPlain(agent);
		
		}
		
		if(goingBackToCity == -1)
		{
			
			if(this.pathToFollowBackToCenter.isEmpty())
			{
				this.pathToFollowBackToCenter.clear();
				try {
					this.pathToFollowBackToCenter = Utils.aStar(Agent.map,agent.coord, this.farm.coord);
				} catch (NoPathException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			
			
			backToVillage(agent);
			
			// take food to the shelter 
			if(this.vegetables > 0 && agent.coord.x == this.farm.coord.x && agent.coord.y == this.farm.coord.y) {
				
				this.vegetables = this.farm.fill(this.vegetables);
				
				goingBackToCity = 0;
				
				field.removeAgentsOnField();
				
			}
			
			/*
			if(agent.coord.x == 180 && agent.coord.y == 250)
			//if(agent.coord == agent.map.foodShelter.coord)
			{
				//System.out.println("Agent at the food shelter");
				vegetables 	= 0;
				goingBackToCity = 0;
				
				
			}*/
		}
		
		if(foundPlain == true)
		{
			
			if(this.timeToGrowOnField>0){
				timeToGrowOnField --;
			}
			
			else
			{
				boolean test = plainToField(agent.coord, agent);
				
				if (test == true)
				{
					vegetables 	= 1;
					goingBackToCity =  -1;
					
				}
				
				this.timeToGrowOnField = Farmer.timeToGrownOnFieldInit;
				foundPlain 	= false;
				
			}
				
				
		}
		
		
	}
	
	
	public boolean findPlain(Agent agent){
		
		//System.out.println("findPlain");
		
		Coordinate c;
		
		if (pathToFollowPlain.isEmpty())
		{
		
			c = this.plainAround(agent);
			if(c != null) 
			{
				//System.out.println("c not null");
				try {
					this.pathToFollowPlain = Utils.aStar(Agent.map,agent.coord, c);
				} catch (NoPathException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				this.pathToFollowPlain = this.pathToFollowPlain.subList(0, this.pathToFollowPlain.size());
			} else {
				
				//System.out.println("c null");
				// Go look for plain
				this.pathToFollowPlain.clear();
			
			
				c = Utils.moveRandomTo(agent, currentDX, currentDY, 3, 7);
				while(!Agent.map.isVisitable(c)) {
					this.currentDX = 1 - 2*Math.random();
					this.currentDY = 1 - 2*Math.random();
					c = Utils.moveRandomTo(agent, currentDX, currentDY, 3, 7);
				}	
	
				this.pathToFollowPlain.add(c);
		
			}
			
		}
		
		
		((Human) agent).move(pathToFollowPlain.get(0));
		this.pathToFollowPlain = pathToFollowPlain.subList(1, pathToFollowPlain.size());
		
		//System.out.println("coordFindPlain:"+agent.coord.x+" "+agent.coord.y);
	
		return true;
	}
	
	private Coordinate plainAround(Agent agent) {
		
		//System.out.println("plainAround");
		Cell result = null;
		double d = Double.MAX_VALUE;
		double tmpD;
		Collection<Cell> view = Agent.map.getCircleAround(agent.coord, visionRadius);
		for(Cell c : view) {
			if((c.topography == Topography.plain && c.isConstructible())
					|| (c.content==Content.building && c.getBuilding().getClass().toString().equals("class buildings.Field"))) {
				tmpD = Math.sqrt(Math.pow(agent.coord.x-c.coord.x, 2) + Math.pow(agent.coord.y-c.coord.y, 2));
				if(tmpD < d && tmpD!=0) {
					d = tmpD;
					result = c;
				}
			}
		}
		//System.out.println("plainAround"+result.coord.x+" "+result.coord.y);
		if(result == null) return null;
		else return result.coord;
	}
	


	public boolean plainToField(Coordinate c, Agent agent){
		
		//System.out.println("Plain to Field");
		if(agent.getCell().content == Content.building && field !=null
				&& field.getAgentsOnField()<field.fieldCapacityMax){
			this.timeToGrowOnField = 3;
			field.addAgentsOnField();
			//System.out.println("Agent on field:::"+field.getAgentsOnField()+":::::"+field.fieldCapacityMax);
			backToVillage(agent);
			return true;
		}

		//Create field
		field = new Field(c,Agent.map);
		
		this.timeToGrowOnField = 3;
		try 
		{
			//System.out.println("Plain to field");
			
			Agent.map.addBuilding(field);
			
			for(int i = field.coord.x;i<field.getXSize();i++)
			{
				for(int j = field.coord.y;j<field.getYSize();j++)
				{
					Agent.map.getCell(new Coordinate(i,j)).content = Content.field;
				
				}
				
			}
			
			field.addAgentsOnField();
			
			backToVillage(agent);
			return true;
			
		} catch (NonConstructibleException e) {
			
			
			//e.printStackTrace();
			return false;
		}
		
		
	}

	
	public void backToVillage(Agent agent){
		
		//System.out.println("back to cityCenter");
		
		this.pathToFollowPlain.clear();
		
		if(!this.pathToFollowBackToCenter.isEmpty())
		{
	
			((Human) agent).move(pathToFollowBackToCenter.get(0));
			this.pathToFollowBackToCenter = pathToFollowBackToCenter.subList(1, pathToFollowBackToCenter.size());
		
			//System.out.println("coordBack2V:"+agent.coord.x+" "+agent.coord.y);
		}

		
	}
	
	public Farm getFoodShelter() {
		return farm;
	}

	public void setFoodShelter(Farm setFarm) {
		this.farm = setFarm;
	}
	
	@Override
	public List<String> getInfo() {
		List<String> current = super.getInfo();
		current.add("Vegetables: " + ((Integer) vegetables).toString());
		return current;
	}
}