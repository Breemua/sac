package agents;

import general.Coordinate;
import general.Utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import map.Map;

public class Wolf extends Animal {

	private static final int reproductionRadius = 10;
	private static final int attackRadius = 3;
	private static final int visionRadius = 6;
	
	private static final int timeMaxBeforeReproductionAttempt = 700;
	
	private static final int lifeExpectancy = 5000;
	
	private int timeBeforeReproductionAttempt;
	
	public Wolf(Coordinate coord, Map map) {
		super(coord, map, lifeExpectancy);
		this.resetReproductionTime();
	}
	
	@Override
	public void run() {
		super.run();
		
		this.move();
		
		if(timeBeforeReproductionAttempt > 0) {
			timeBeforeReproductionAttempt --;
		}
		
	}
	
	private void resetReproductionTime() {
		timeBeforeReproductionAttempt = (int) (0.5 + Math.random())*timeMaxBeforeReproductionAttempt;
	}
	
	private void addTimeToReproductionTime(int time) {
		timeBeforeReproductionAttempt += time;
	}
	
	public void move() {
		Comparator<Agent> c = new Comparator<Agent>() {
			@Override
			public int compare(Agent o1, Agent o2) {
				double d1 = o1.coord.getSquareDistanceFrom(coord);
				double d2 = o2.coord.getSquareDistanceFrom(coord);
				if(d1 < d2) {
					return -1;
				} else if(d1 == d2) {
					return 0;
				} else {
					return 1;
				}
			}
		};
		
		Collection<Agent> agentsInReproductionRadius = this.getAgentsAround(reproductionRadius);
		List<Wolf> wolfsInReproductionRadius = new ArrayList<Wolf>();
		for(Agent a : agentsInReproductionRadius) {
			if(a instanceof Wolf) {
				wolfsInReproductionRadius.add((Wolf) a);
			}
		}
		Collections.sort(wolfsInReproductionRadius, c);
		
		Collection<Agent> agentsInAttackRadius = this.getAgentsAround(attackRadius);
		List<Human> humanInAttackRadius = new ArrayList<Human>();
		for(Agent a : agentsInAttackRadius) {
			if(a instanceof Human) {
				humanInAttackRadius.add((Human) a);
			}
		}
		Collections.sort(humanInAttackRadius, c);
		
		Collection<Agent> agentsInRunAwayRadius = this.getAgentsAround(visionRadius);
		List<Human> humanInRunAwayRadius = new ArrayList<Human>();
//		for(Agent a : agentsInRunAwayRadius) {
//			if(a instanceof Human) {
//				humanInRunAwayRadius.add((Human) a);
//			}
//		}
//		Collections.sort(humanInRunAwayRadius, c);
		
		
		Coordinate destination = null;
		
		if(destination == null && !humanInAttackRadius.isEmpty()) {
			
			int xAverage = 0;
			int yAverage = 0;
			
			int cpt = 0;
			for(Human h : humanInAttackRadius) {
				xAverage += h.coord.x;
				yAverage += h.coord.y;
				cpt++;
				if(cpt >= 5) break;
			}
			xAverage /= cpt;
			yAverage /= cpt;
			
			destination = Utils.moveRandomTo(this, -coord.x + xAverage, 
												   -coord.y + yAverage, 3, 1);
		}
		
		if(destination == null && !humanInRunAwayRadius.isEmpty()) {
			int xAverage = 0;
			int yAverage = 0;
			
			int cpt = 0;
			for(Human h : humanInRunAwayRadius) {
				xAverage += h.coord.x;
				yAverage += h.coord.y;
				cpt++;
				if(cpt >= 5) break;
			}
			xAverage /= cpt;
			yAverage /= cpt;
			
			destination = Utils.moveRandomTo(this, coord.x - xAverage, 					
												   coord.y - yAverage, 1, 1);
		} 
		
		if(timeBeforeReproductionAttempt == 0) {
			Collection<Agent> agentInCell = Agent.map.getCell(this.coord).getAgents();
			
			Wolf pair = null;
			for(Agent a : agentInCell) {
				if(a instanceof Wolf) {
					if(a != this) {
						pair = (Wolf) a;
					}
				}
			}
			if(pair != null) {
				this.reproductWith(pair);
			} else {
				if(destination == null && wolfsInReproductionRadius.size() > 1) {
					destination = Utils.moveRandomTo(this, -coord.x + wolfsInReproductionRadius.get(1).coord.x, 
													   	-coord.y + wolfsInReproductionRadius.get(1).coord.y, 5, 1);
				}
			}
			
		}
		
		while(destination == null || !Agent.map.getCell(destination).isVisitableInForest()){
			destination = Utils.moveRandom(this, 1);
		}
		
		this.move(destination);
	}

	
	private void reproductWith(Wolf wolf) {
//		System.out.println("Reproduction attempt");
		this.resetReproductionTime();
		wolf.resetReproductionTime();
		if(Math.random() > 0.9) {
//			System.out.println("Reproduction success");
			Wolf newWolf = new Wolf(this.coord, Agent.map);
			newWolf.addTimeToReproductionTime(3*this.timeBeforeReproductionAttempt);
			Agent.map.addAgent(newWolf);
		}
	}
	
	public int encounters(Agent human) {
		double random = Math.random();
		if(random<0.9) {
			this.die();
			return 15;//quantity of meat
		} else {
			return -1;
		}
	}

	@Override
	public List<String> getInfo() {
		List<String> string = super.getInfo();
		string.add("Time before reproduction: " + this.timeBeforeReproductionAttempt);
		return string;
	}
	
}
