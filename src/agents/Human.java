package agents;

import general.Coordinate;

import java.awt.Color;
import java.util.List;

import map.Map;

public class Human extends MovingAgent {

	private static int lowFoodLevel = 50;
	private static int lowSleepLevel = 30;
	
	protected int food;
	protected int sleep;
	protected Role role;
	
	public Human(Coordinate coord, Map map) {
		super(coord, map, Integer.MAX_VALUE);
		this.role = null;
	}

	@Override
	public void run() {
		super.run();
		if(this.sleep <= lowSleepLevel) {
			//TODO Implement
//			System.out.println("Low sleep level");
		}
		if(this.food <= lowFoodLevel) {
			//TODO Implement
//			System.out.println("Low food level");
		}
		
		role.run(this);
	}
	
	public void eat(int amount) {
		food += amount;
	}
	
	public void sleep(int recuperation) {
		sleep += recuperation;
	}
	
		
	public void setRole(Role role) {
		this.role = role;
	}
	
	public void setMissionTime(int time) {
		role.setMissionTime(time);
	}

	public void restart() {
		role.restart();
	}

	public Role getRole() {
		return this.role;
	}

	@Override
	public List<String> getInfo() {
		List<String> current = super.getInfo();
		current.add("Food: " + ((Integer) food).toString());
		current.add("Sleep: " + ((Integer) sleep).toString());
		current.add("Role: " + role.getClass().getName());
		current.addAll(role.getInfo());
		return current;
	}
	
	@Override
	public Color getColor() {
		return role.getColor();
	}
	
}
