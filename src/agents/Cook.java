package agents;

import exceptions.NoPathException;
import general.Coordinate;
import general.Utils;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import buildings.FoodShelter;
import buildings.Kitchen;
import buildings.WaterTank;


public class Cook extends Role {

	private int waterAmount;
	private int foodAmount;
	private WaterTank waterTank;
	private FoodShelter foodShelter;
	private Kitchen kitchen;
	
	private static int lowWaterLevel = 10;
	private static int lowFoodLevel = 10;
	
	private static int waterCapacity = 5;
	private static int foodCapacity = 5;
	
	
	public Cook () {
		super();
		this.waterAmount = 0;
		this.foodAmount = 0;
		this.pathToFollow = new ArrayList<Coordinate>();
	}
	
	public void run(Agent agent) {
		
		super.run(agent);
				
		if(agent.coord.equals(this.waterTank.coord)) {
			this.waterAmount = this.waterTank.drain(waterCapacity);
		} else if(agent.coord.equals(this.foodShelter.coord)) {
			this.foodAmount = this.foodShelter.drain(foodCapacity);
		} else if(agent.coord.equals(this.kitchen.coord)) {
			if(this.waterAmount > 0) {
				this.waterAmount = this.kitchen.fillWater(this.waterAmount);
			}
			
			if(this.foodAmount > 0) {
				this.foodAmount = this.kitchen.fillFood(this.foodAmount);
			}
			
			this.kitchen.cook();

		}
		
		this.move(agent);
		
		
		if(!pathToFollow.isEmpty()) {
			((Human) agent).move(pathToFollow.get(0));
			this.pathToFollow = pathToFollow.subList(1, pathToFollow.size());
		}
		
	}

	private void move(Agent agent) {
		if(this.foodAmount == 0 && this.waterAmount == 0 &&
				!agent.coord.equals(this.waterTank.coord) &&
				!agent.coord.equals(this.foodShelter.coord)) {
			
			if(pathToFollow.isEmpty()) {
				if(this.kitchen.getWaterLevel() < lowWaterLevel) {
					try {
						this.pathToFollow = Utils.aStar(Agent.map, agent.coord, this.waterTank.coord);
					} catch (NoPathException e) {
						e.printStackTrace();
					}
				}
			}
			
			if(pathToFollow.isEmpty()) {
				if (this.kitchen.getFoodLevel() < lowFoodLevel) {
					try {
						this.pathToFollow = Utils.aStar(Agent.map, agent.coord, this.foodShelter.coord);
					} catch (NoPathException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		if(pathToFollow.isEmpty()) {
			if(agent.coord.equals(this.kitchen.coord)) {
				kitchen.cook();
			} else {
				try {
					this.pathToFollow = Utils.aStar(Agent.map, agent.coord, this.kitchen.coord);
				} catch (NoPathException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void setWaterTank(WaterTank waterTank) {
		this.waterTank = waterTank;
	}

	public void setFoodShelter(FoodShelter foodShelter) {
		this.foodShelter = foodShelter;
	}

	public void setKitchen(Kitchen kitchen) {
		this.kitchen = kitchen;
	}
	
	@Override
	public List<String> getInfo() {
		List<String> current = super.getInfo();
		current.add("Water: " + ((Integer) waterAmount).toString());
		current.add("Food: " + ((Integer) foodAmount).toString());
		return current;
	}
	
	@Override
	public Color getColor() {
		return new Color(60, 60, 60);
	}

}
