package agents;

import general.Coordinate;
import general.Main;
import general.Utils;

import java.awt.Color;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import map.Cell;
import map.Topography;

public class Explorer extends Role {

	private Map<Coordinate, Integer> waterCoordinates;
//	private Set<Coordinate> forestCoordinates;
	
	private double currentDX;
	private double currentDY;
	
	public Explorer() {
		super();
		this.waterCoordinates = new HashMap<Coordinate, Integer>();
//		this.forestCoordinates = new HashSet<Coordinate>();
		this.currentDX = 1 - 2*Math.random();
		this.currentDY = 1 - 2*Math.random();
	}
	
	@Override
	public void run(Agent agent) {
		super.run(agent);
		
		Coordinate to;
		if(this.pathToFollow.isEmpty()) {
//			if(toTownCenter) { // We are changing the exploration direction if the agent went home.
//				System.out.println("CHANGING");
//				this.currentDX = 1 - 2*Math.random();
//				this.currentDY = 1 - 2*Math.random();
//			}
			this.fillForestAndWaterAround(agent);
			to = Utils.moveRandomTo(agent, currentDX, currentDY, 3, 1);
			while(!Agent.map.isVisitable(to)) {
				this.currentDX = 1 - 2*Math.random();
				this.currentDY = 1 - 2*Math.random();
				to = Utils.moveRandomTo(agent, currentDX, currentDY, 3, 1);
			}	
		} else {
			to = pathToFollow.get(0);
			this.pathToFollow = pathToFollow.subList(1, pathToFollow.size());
		}
		((Human) agent).move(to);
	}
	
	private void fillForestAndWaterAround(Agent agent) {
		Collection<Cell> around = Agent.map.getSquareAround(agent.coord, 1);
		for(Cell cell : around) {
//			if(cell.content == Content.forest) {
//				forestCoordinates.add(cell.coord);
//			} else 
				if(cell.topography == Topography.water) {
					waterCoordinates.put(agent.coord, Main.round);
			}
		}
	}

	public void reset() {
		this.currentDX = 1 - 2*Math.random();
		this.currentDY = 1 - 2*Math.random();
		this.waterCoordinates.clear();
	}

	public Set<Coordinate> getWaterCoordinates() {
		return this.waterCoordinates.keySet();
	}
	
	@Override
	public List<String> getInfo() {
		List<String> infos = super.getInfo();
		for(Coordinate c : this.waterCoordinates.keySet()) {
			infos.add("Water position: " + c);
		}
		return infos;
	}
	
	@Override
	public Color getColor() {
		return new Color(0, 100, 0);
	}
	
	
}
