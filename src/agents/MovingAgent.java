package agents;

import general.Coordinate;

import java.awt.Color;

import map.Cell;
import map.Map;

public class MovingAgent extends Agent {

	
	public MovingAgent(Coordinate coord, Map map, int lifeExpectancy) {
		super(coord, map, lifeExpectancy);
	}

	public void move(Coordinate coord) {
		Cell oldCell = map.getCell(this.coord);
		Cell newCell = map.getCell(coord);
		oldCell.removeAgent(this);
		newCell.addAgent(this);
		this.coord = coord;
	}
	
	@Override
	public Color getColor() {
		return new Color(255, 0, 0);
	}
	
	
}
