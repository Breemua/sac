package agents;

import general.Coordinate;

import java.awt.Color;

import map.Map;

public class Animal extends MovingAgent {
	
	public Animal(Coordinate coord, Map map, int lifeExpectancy) {
		super(coord, map, lifeExpectancy);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
	}

	public int encounters(Agent a) {
		return -1;
	}
	
	@Override
	public Color getColor() {
		return new Color(180, 150, 100);
	}
}
