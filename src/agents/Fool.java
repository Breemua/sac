package agents;

import general.Coordinate;
import map.Map;

public class Fool extends Role {

	public Fool() {
		super();
	}
	
	@Override
	public void run(Agent agent) {
		this.moveRandom(agent, 1);
	}
	
	public void moveRandom(Agent agent, int step) {
		double move = Math.random();
		Map map = Agent.map;
		Coordinate coord = agent.coord;
		
		Coordinate to = null;
		if(move<1./8) {
			to = new Coordinate(coord.x+step, coord.y+step);
		} else if(move < 2./8) {
			to = new Coordinate(coord.x+step, coord.y);
		} else if(move < 3./8) {
			to = new Coordinate(coord.x+step, coord.y-step);
		} else if(move < 4./8) {
			to = new Coordinate(coord.x, coord.y+step);
		} else if(move < 5./8) {
			to = new Coordinate(coord.x, coord.y-step);
		} else if(move < 6./8) {
			to = new Coordinate(coord.x-step, coord.y+step);
		} else if(move < 7./8) {
			to = new Coordinate(coord.x-step, coord.y);
		} else {
			to = new Coordinate(coord.x-step, coord.y-step);
		}
		to.normalize(map);
		if(map.getCell(to).isVisitable()) {
			((Human) agent).move(to);
		}
	}

}
