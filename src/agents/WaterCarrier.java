package agents;

import exceptions.NoPathException;
import general.Coordinate;
import general.Utils;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import map.Cell;
import map.Map;
import map.Topography;
import buildings.WaterTank;

public class WaterCarrier extends Role {

	private static final int visionRadius = 15;
	private int amountWater;
	private Coordinate knownWaterLocation;
	private WaterTank waterTank;
	
	public WaterCarrier() {
		super();
		this.amountWater = 0;
		this.knownWaterLocation = null;
		this.pathToFollow = new ArrayList<Coordinate>();
	}
	
	@Override
	public void run(Agent agent) {
		super.run(agent);
		
//		System.out.println(this.amountWater);
		
		if(this.amountWater > 0 && agent.coord.equals(this.waterTank.coord)) {
			this.amountWater = this.waterTank.fill(this.amountWater);
		}

		if(agent.coord.equals(this.knownWaterLocation)) {
			this.amountWater += 5;
		}
		
//		System.out.println(pathToFollow.isEmpty());
		
		try {
			if(pathToFollow.isEmpty()) {
				if(this.amountWater > 0) {
					this.pathToFollow = Utils.aStar(Agent.map, agent.coord, this.waterTank.coord);
//					this.pathToFollow = Utils.bresenham(agent.coord, this.waterTank.coord);
				} else if(this.knownWaterLocation != null) {
					this.pathToFollow = Utils.aStar(Agent.map, agent.coord, this.knownWaterLocation);
//					this.pathToFollow = Utils.bresenham(agent.coord, this.knownWaterLocation);
				} else if(agent.coord.equals(Map.townCenter.coord)) {
					Map.townCenter.communicateWith((Human) agent);
				} else {
					this.pathToFollow = Utils.aStar(Agent.map, agent.coord, Map.townCenter.coord);
//					this.pathToFollow = Utils.bresenham(agent.coord, Map.townCenter.coord);
				}
			}
		} catch (NoPathException e) {
			e.printStackTrace();
		}

//		if(pathToFollow.isEmpty()) {
//			if(this.amountWater > 0) {
//				this.pathToFollow = Utils.bresenham(agent.coord, this.waterTank.coord);
//			} else {
//			Coordinate c = this.waterNext(agent);
//			if(c != null) {
//				this.amountWater = 5;
////				this.pathToFollow = path to the towncenter
//				this.pathToFollow = Utils.bresenham(agent.coord, this.waterTank.coord);
////				Agent.pathFinder.calcShortestPath(agent.coord, this.waterTank.getUpperLeft()).getPath();
//			} else {
//				if(this.knownWaterLocation != null) {
////					this.pathToFollow = shortest path to the water location
//					this.pathToFollow = Utils.bresenham(agent.coord, this.knownWaterLocation);
////					Agent.pathFinder.calcShortestPath(agent.coord, this.knownWaterLocation).getPath();
//
//				} else {
//					c = this.waterAround(agent);
//					if(c != null) {
//						this.knownWaterLocation = c;
////						this.pathToFollow = path to the water
//						this.pathToFollow = Utils.bresenham(agent.coord, c);
////						Agent.pathFinder.calcShortestPath(agent.coord, c).getPath();
//						this.pathToFollow = this.pathToFollow.subList(0, this.pathToFollow.size()-1);
//					} else {
//						c = this.getWaterFromOther(agent);
//						if(c != null) {
//							this.knownWaterLocation = c;
////							this.pathToFollow = path to the water
//							this.pathToFollow = Utils.bresenham(agent.coord, c);
////									Agent.pathFinder.calcShortestPath(agent.coord, c).getPath();
//						} else {
//							// Go look for water
//							this.pathToFollow.clear();
//							do {
////								c = Utils.moveRandom(agent, 1);
//								c = Utils.moveRandomTo(agent, 1., 0., 2, 1);
//							} while (!Agent.map.isVisitable(c));
//							
//							this.pathToFollow.add(c);
//						}
//					}
//				}
//			}
//		}}
		
		if(!pathToFollow.isEmpty()) {
			((Human) agent).move(pathToFollow.get(0));
			this.pathToFollow = pathToFollow.subList(1, pathToFollow.size());
		}
		
	}

	
	
	private Coordinate waterAround(Agent agent) {
		Cell result = null;
		double d = Double.MAX_VALUE;
		double tmpD;
		Collection<Cell> view = Agent.map.getCircleAround(agent.coord, visionRadius);
		for(Cell c : view) {
			if(c.topography == Topography.water) {
				tmpD = Math.sqrt(Math.pow(agent.coord.x-c.coord.x, 2) + Math.pow(agent.coord.y-c.coord.y, 2));
				if(tmpD < d) {
					d = tmpD;
					result = c;
				}
			}
		}
		if(result == null) return null; else return result.coord;
	}

	private Coordinate waterNext(Agent agent) {
		Collection<Cell> cellAround = Agent.map.getSquareAround(agent.coord, 1);
		for(Cell c : cellAround) {
			if(c.topography == Topography.water) {
				return c.coord;
			}
		}
		return null;
	}
	
	private Coordinate getWaterFromOther(Agent agent) {
		Collection<Cell> view = Agent.map.getCircleAround(agent.coord, visionRadius);
		for(Cell c : view) {
			for(Agent h : c.getAgents()) {
				if(h instanceof Human && ((Human)h).role instanceof WaterCarrier) {
					return ((WaterCarrier) ((Human)h).role).knownWaterLocation;
				}
			}
		}
		return null;
	}

	public WaterTank getWaterTank() {
		return waterTank;
	}

	public void setWaterTank(WaterTank waterTank) {
		this.waterTank = waterTank;
	}
	
	/**
	 * @return the knownWaterLocation
	 */
	public Coordinate getKnownWaterLocation() {
		return knownWaterLocation;
	}

	/**
	 * @param knownWaterLocation the knownWaterLocation to set
	 */
	public void setKnownWaterLocation(Coordinate knownWaterLocation) {
		this.knownWaterLocation = knownWaterLocation;
	}
	
	
	@Override
	public List<String> getInfo() {
		List<String> current = super.getInfo();
		current.add("Water: " + ((Integer) amountWater).toString());
		current.add("Water Location: " + knownWaterLocation);
		return current;
	}
	
	@Override
	public Color getColor() {
		return new Color(0, 0, 255);
	}
		
}
