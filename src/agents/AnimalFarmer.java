package agents;

import exceptions.NoPathException;
import general.Utils;
import buildings.AnimalStable;

public class AnimalFarmer extends Role {
	
	private AnimalStable animalStable;

	public AnimalFarmer() {
		super();
	}
	
	public void setAnimalStable(AnimalStable animalStable) {
		this.animalStable = animalStable;
	}
	
	@Override
	public void run(Agent agent) {
		super.run(agent);
		
		if(this.pathToFollow.isEmpty()) {
			if(agent.coord.equals(animalStable.coord)) {
				animalStable.work();
			} else {
				try {
					this.pathToFollow = Utils.aStar(Agent.map, agent.coord, animalStable.coord);
				} catch (NoPathException e) {
					e.printStackTrace();
				}
			}
		}
		if(!this.pathToFollow.isEmpty()) {
			((Human) agent).move(pathToFollow.get(0));
			this.pathToFollow = pathToFollow.subList(1, pathToFollow.size());
		}
	}
	
}
