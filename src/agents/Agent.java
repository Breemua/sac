package agents;

import general.Coordinate;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import map.Cell;
import map.Map;

public abstract class Agent {

	public Coordinate coord;
	protected int life;
	protected int lifeExpectancy;
	public static Map map;
	
	public Agent(Coordinate coord, Map map, int lifeExpectancy) {
		this.coord = coord;
		this.life = 0;
		this.lifeExpectancy = lifeExpectancy;
		if(Agent.map == null) Agent.map = map;
	}
	
	public void run() {
		life++;
		if(life >= lifeExpectancy) {
			double criteria = Math.abs(Math.random()*Math.random()*Math.random());
			criteria += 1 - 1/Math.exp((life-lifeExpectancy)/50);
			if(criteria >= 0.95) {
				System.out.println(this + " is dead");
				this.die();
			}
		}
	}
	
	public void die() {
		map.removeAgent(this);
		map.removeAgent(this);
	}
	
	public Color getColor() {
		return new Color(0, 0, 0);
	}

	public Cell getCell() {
		return Agent.map.getCell(coord);		
	}
	
	protected Collection<Agent> getAgentsAround(int radius) {
		Collection<Cell> cells = Agent.map.getCircleAround(coord, radius);
		Collection<Agent> result = new ArrayList<Agent>();
		for(Cell c : cells) {
			result.addAll(c.getAgents());
		}
		return result;
	}
	
	protected Human getClosestHumanAround(int radius) {
		double distance = Double.MAX_VALUE;
		Human currentAgent = null;
		double tmpDistance = Double.MAX_VALUE;
		for(Agent a : this.getAgentsAround(radius)) {
			if(a instanceof Human) {
				tmpDistance = a.coord.getSquareDistanceFrom(coord);
				if(tmpDistance < distance) {
					distance = tmpDistance;
					currentAgent = (Human) a;
				}
			}
		}
		return currentAgent;
	}
	
	protected Collection<Agent> getAgentsNext() {
		Collection<Cell> cells = Agent.map.getSquareAround(coord, 1);
		Collection<Agent> result = new ArrayList<Agent>();
		for(Cell c : cells) {
			result.addAll(c.getAgents());
		}
		return result;
	}
	
	public List<String> getInfo() {
		List<String> string = new ArrayList<String>();
		string.add("Coord: " + coord.toString());
		string.add("Life: " + life + "/" + lifeExpectancy);
		return string;
	}
	
}
