package general;

import map.Map;

public class Coordinate {

	public int x;
	public int y;
	
	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void normalize(Map map) {
		if(this.x < 0) this.x = 0;
		if(this.x >= map.getXResolution()) this.x = map.getXResolution()-1;
		if(this.y < 0) this.y = 0;
		if(this.y >= map.getYResolution()) this.y = map.getYResolution()-1;
	}

	public double getSquareDistanceFrom(Coordinate c) {
		return Math.pow(c.x - x, 2) + Math.pow(c.y - y, 2);
	}
	
	
	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordinate other = (Coordinate) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	
	
}
