package general;

import gui.Window;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import map.Map;
import agents.Agent;

public class Main {

	public static int round = 0;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Map map = new Map(400, 400);
		Window screen = new Window(500, 500, map);
		
		while(true) {
			
//			System.out.println("Round " + round);
			round++;
			
			for(Agent agent : new ArrayList<Agent>(map.getAgents())) {
				agent.run();
			}

			BufferedImage bu = map.toBufferedImage();
//			try {
//				List<Coordinate> c = Utils.aStar(map, new Coordinate(110, 110), new Coordinate(180, 212));
//				for(Coordinate cc : c) {
//					bu.setRGB(cc.x, cc.y, (new Color(255, 0, 0)).getRGB());
//				}
//			} catch (NoPathException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//			bu.setRGB(110, 110, (new Color(255, 255, 0)).getRGB());
//			bu.setRGB(180, 212, (new Color(255, 255, 0)).getRGB());
			screen.display(bu);
			screen.updateList();
			
			
//			try {
//				Thread.sleep(100);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
			
		}
			
//		System.out.println(ImprovedNoise.noise(0.05, 0.05, 0.02));
	}

}
