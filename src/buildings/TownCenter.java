package buildings;

import general.Coordinate;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import map.Map;
import agents.Explorer;
import agents.Human;
import agents.WaterCarrier;

/**
 * May replace the chief
 *
 */
public class TownCenter extends Building {
		
	private Set<Coordinate> waterPositions;
//	private Set<Coordinate> ForestPosition;
//	private List<WaterTank> waterTanks;
	
	public TownCenter(Coordinate upperLeft, Map map) {
		super(upperLeft, 7, 5, map, 10000);
		this.waterPositions = new HashSet<Coordinate>();
//		this.waterTanks = new ArrayList<WaterTank>();
	}

	@Override
	public void communicateWith(Human agent) {
		
		if(agent.getRole() instanceof Explorer) {
			agent.setMissionTime(300);
			waterPositions.addAll(((Explorer) agent.getRole()).getWaterCoordinates());
			((Explorer) agent.getRole()).reset();
		} else if(agent.getRole() instanceof WaterCarrier) {
			if(!this.waterPositions.isEmpty()) {
			((WaterCarrier) agent.getRole()).setKnownWaterLocation(
					this.getClosestWaterPositionFrom(
							((WaterCarrier) agent.getRole()).getWaterTank().coord));
			}
			agent.setMissionTime(300);
		} else agent.setMissionTime(300);
	}
	
	
	private Coordinate getClosestWaterPositionFrom(Coordinate coord) {
		double distTmp = Double.MAX_VALUE;
		double dist = Double.MAX_VALUE;
		Coordinate cc = null;
		for(Coordinate c : waterPositions) {
			distTmp = c.getSquareDistanceFrom(coord);
			if(distTmp < dist) {
				dist = distTmp;
				cc = c;
			}
		}
		return cc;
	}

	@Override
	public List<String> getInfo() {
		List<String> current = super.getInfo();
		for(Coordinate c : this.waterPositions) {
			current.add("WaterPosition: " + c);
		}
		return current;	
	}
	
}
