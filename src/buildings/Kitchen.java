package buildings;

import general.Coordinate;

import java.util.List;

import map.Map;
import agents.Human;


public class Kitchen extends Stack {

	private int foodLevel;
	private int foodLevelMax;
	private int waterLevel;
	private int waterLevelMax;

	public Kitchen(Coordinate upperLeft, int levelMax, Map map) {
		super (upperLeft, levelMax, 0, map);
		foodLevel = 0;
		waterLevel = 0;
		foodLevelMax = levelMax/2;
		waterLevelMax = levelMax/2;
	}

	public int fillWater(int amount) {
		if(waterLevel + amount <= waterLevelMax) {
			waterLevel += amount;
			return 0;
		} else {
			waterLevel = waterLevelMax;
			return waterLevel + amount - waterLevelMax;
		}
		
	}
	
	public int fillFood(int amount) {
		if(foodLevel + amount <= foodLevelMax) {
			foodLevel += amount;
			return 0;
		} else {
			foodLevel = foodLevelMax;
			return foodLevel + amount - foodLevelMax;
		}
		
	}
	
	public int drainWater(int amount) {
		if(waterLevel - amount >= 0) {
			waterLevel -= amount;
			return amount;
		} else {
			int l = waterLevel;
			waterLevel = 0;
			return l;
		}
	}
	
	public int drainFood(int amount) {
		if(foodLevel - amount >= 0) {
			foodLevel -= amount;
			return amount;
		} else {
			int l = foodLevel;
			foodLevel = 0;
			return l;
		}
	}
	
	@Override
	public void communicateWith(Human agent) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public List<String> getInfo() {
		List<String> current = super.getInfo();
		current.add("Food level: " + foodLevel + " / " + foodLevelMax);
		current.add("Water level: " + waterLevel + " / " + waterLevelMax);
		return current;	
	}

	public int getFoodLevel() {
		return foodLevel;
	}

	public void setFoodLevel(int foodLevel) {
		this.foodLevel = foodLevel;
	}

	public int getWaterLevel() {
		return waterLevel;
	}

	public void setWaterLevel(int waterLevel) {
		this.waterLevel = waterLevel;
	}

	public boolean isCookable() {
		return this.foodLevel > 0 && this.waterLevel > 0;
	}
	
	public void cook() {
		if(this.isCookable() && 
				this.level + 1 <= this.foodLevelMax) {	
			this.drainFood(1);
			this.drainWater(1);
			this.fill(1);
		}
	}
	
	

}
