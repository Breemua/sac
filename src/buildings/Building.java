package buildings;

import general.Coordinate;

import java.util.List;

import map.Map;
import agents.Agent;
import agents.Human;


// In fact, a building can be deteriorated by natural hazards, it do have
// have a life and a life expectancy. Thus the inheritance to Agent.
public abstract class Building extends Agent {

	private int xSize;
	private int ySize;

	public Building(Coordinate upperLeftCoord, int xSize, int ySize, 
			Map map, int lifeExpectancy) {
		super(upperLeftCoord, map, lifeExpectancy);
		this.setXSize(xSize);
		this.setYSize(ySize);
	}

	public int getXSize() {
		return xSize;
	}

	public void setXSize(int xSize) {
		this.xSize = xSize;
	}

	public int getYSize() {
		return ySize;
	}

	public void setYSize(int ySize) {
		this.ySize = ySize;
	}

	public abstract void communicateWith(Human agent);
	
	
	@Override
	public List<String> getInfo() {
		List<String> current = super.getInfo();
		current.add("Size: " + xSize + " x " + ySize);
		return current;	
	}

}
