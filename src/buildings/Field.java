package buildings;

import general.Coordinate;
import map.Map;
import agents.Human;

public class Field extends Building {

	public int fieldCapacityMax = 2;
	int agentsOnField;

	public Field(Coordinate upperLeft, Map map) {
	super(upperLeft, 3, 3, map, 100000);
	
	this.agentsOnField = 1;
	
	}
	
	public void clear(){
		this.agentsOnField = 0;
	}
	
	public boolean removeAgentsOnField(){
		
		this.agentsOnField--;
		return true;
	}

	public boolean addAgentsOnField(){
		
		this.agentsOnField++;
		return true;
	}
	
	public int getAgentsOnField(){
		
		return this.agentsOnField;
	}
	
	@Override
	public void communicateWith(Human agent) {
		// TODO Auto-generated method stub
		
	}
}
