package buildings;

import general.Coordinate;

import java.util.List;

import map.Map;
import agents.Human;

public class AnimalStable extends FoodShelter {

	private int animalCount;
	private int counter;
	private static int maxCounter = 50;
	
	public AnimalStable(Coordinate upperLeft, int levelMax, Map map) {
		super(upperLeft, levelMax, 0, map);
		this.animalCount = 0;
		counter = 0;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void communicateWith(Human agent) {
		// TODO Auto-generated method stub

	}

	public void addAnimal() {
		this.animalCount++;
	}

	@Override
	public List<String> getInfo() {
		List<String> current = super.getInfo();
		current.add("Animal count: " + animalCount);
		return current;	
	}

	public void work() {
		if(counter == maxCounter) {
			this.fill(animalCount);
			this.counter = 0;
		} else {
			this.counter++;
		}
	}
	
}
