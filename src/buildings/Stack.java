package buildings;

import general.Coordinate;

import java.util.List;

import map.Map;

public abstract class Stack extends Building{
	
	
	protected int levelMax;
	protected int level;
	

	public Stack(Coordinate upperLeft, int levelMax, int level, Map map) {
		super(upperLeft, 2, 2, map, 5000);
		this.levelMax = levelMax;
		this.level = level;
	}
	
	public int fill(int amount) {
		if(level + amount <= levelMax) {
			level += amount;
			return 0;
		} else {
			level = levelMax;
			return level + amount - levelMax;
		}
		
	}
	
	public int drain(int amount) {
		if(level - amount >= 0) {
			level -= amount;
			return amount;
		} else {
			int l = level;
			level = 0;
			return l;
		}
	}

	/**
	 * @return the levelMax
	 */
	public int getLevelMax() {
		return levelMax;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}
	
	
	@Override
	public List<String> getInfo() {
		List<String> current = super.getInfo();
		current.add("Level: " + level + " / " + levelMax);
		return current;	
	}

}
