package buildings;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import agents.Human;

public class StackTest {

	@Test
	public void test() {
		Stack stack = new Stack(null, 0, 100, null) {
			
			@Override
			public void communicateWith(Human agent) {
				// TODO Auto-generated method stub
				
			}
		};
		
		assertEquals(stack.fill(10), 0);
		assertEquals(stack.getLevel(), 10);
		assertEquals(stack.getLevelMax(), 100);
		
	}

}
