package map;

public class NoiseWrapper {

	public static double noise(double x, double y, double z,
							   int octaves, double frequences, double amplitude, 
							   double seedX, double seedY, double seedZ, double seed) {
		
		double t = seed;
		for(int level=1; level<=octaves; level++) {
			t += (amplitude/level)*ImprovedNoise.noise(seedX+frequences*level*x,
													   seedY+frequences*level*y,
													   seedZ+frequences*level*z);
		}

		return t;
	}

}
