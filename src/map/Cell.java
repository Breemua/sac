package map;

import general.Coordinate;

import java.util.HashSet;
import java.util.Set;

import agents.Agent;
import buildings.Building;

public class Cell {
	public Coordinate coord;
	public Topography topography;
	public Content content;
	private Set<Agent> agents;
	
	public Cell(Coordinate c) {
		this.coord = c;
		this.agents = new HashSet<Agent>();
	}
	
	/**
	 * @return the agents
	 */
	public Set<Agent> getAgents() {
		return agents;
	}

	public boolean isVisitable() {
		return topography != Topography.water && 
				topography != Topography.sea &&
				topography != Topography.mountain;
	}
	
	public boolean isConstructible() {
		return isVisitable() && 
				content == Content.empty;
	}
	
	public void setBuilding(Building building) {
		if(this.content == Content.building) {
			System.out.println("A building is already there");
		} else {
			this.agents.add(building);
			this.content = Content.building;
		}		
	}
	
	public Building getBuilding() {
		if(this.content == Content.building) {
			for(Agent a : this.agents) {
				if(a instanceof Building) {
					return (Building) a;
				}
			}
		}
		return null;
	}
	
	public void removeAgent(Agent agent) {
		this.agents.remove(agent);
	}
	
	public void addAgent(Agent agent) {
		this.agents.add(agent);
	}

	public boolean isVisitableInForest() {
		return content == Content.forest && topography == Topography.plain;
	}
		 
}
