package map;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import agents.Agent;
import agents.AnimalFarmer;
import agents.Cook;
import agents.Explorer;
import agents.Farmer;
import agents.Human;
import agents.Hunter;
import agents.MovingAgent;
import agents.Sheep;
import agents.WaterCarrier;
import agents.Wolf;
import buildings.AnimalStable;
import buildings.Building;
import buildings.Farm;
import buildings.Kitchen;
import buildings.TownCenter;
import buildings.WaterTank;
import exceptions.NonConstructibleException;
import general.Coordinate;


public class Map {

	private Cell[][] map;
	private List<Agent> agents;
	private List<Agent> allAgents;
	public static Building townCenter;
	private int xResolution;
	private int yResolution;
	
	public Map(int xResolution, int yResolution) {
		this.agents = new ArrayList<Agent>();
		this.allAgents = new ArrayList<Agent>();
		this.xResolution = xResolution;
		this.yResolution = yResolution;
		this.initializeMap();
		this.generateTopography();
		this.generateForests();
	
		this.initializeAgents();
		this.waterToSea();
	}
	
	public Collection<Cell> getSquareAround(int x, int y, int radius) {
		
		Collection<Cell> result = new ArrayList<Cell>();
		
		int startPosX = (x-radius<0)? 0: x-radius;
	    int startPosY = (y-radius<0)? 0: y-radius;
	    int endPosX = (x+radius>=xResolution)? xResolution-1: x+radius;
	    int endPosY = (y+radius>=yResolution)? yResolution-1: y+radius;
	    
	    for(int i=startPosX; i<=endPosX; i++){
	        for(int j=startPosY; j<=endPosY; j++){
	        	result.add(map[i][j]);
	        }
	    }
	    
	    
	    return result;
	    
	}
	
	public void addBuilding(Building building) throws NonConstructibleException {
		Coordinate upperleft = building.coord;
		for(int i=0; i<building.getXSize(); i++) {
			for(int j=0; j<building.getYSize(); j++) {
				if(!map[upperleft.x + i][upperleft.y + j].isConstructible()) {
					throw new NonConstructibleException();
				}
			}
		}
		for(int i=0; i<building.getXSize(); i++) {
			for(int j=0; j<building.getYSize(); j++) {
				map[upperleft.x + i][upperleft.y + j].setBuilding(building);
			}
		}
		this.allAgents.add(building);
	}
	

	public Collection<Cell> getSquareAround(Coordinate coord, int radius) {
		return this.getSquareAround(coord.x, coord.y, radius);
	}
	
	public Collection<Cell> getCircleAround(int x, int y, int radius) {
		
		Collection<Cell> result = new ArrayList<Cell>();
		
		int startPosX = (x-radius<0)? 0: x-radius;
	    int startPosY = (y-radius<0)? 0: y-radius;
	    int endPosX = (x+radius>=xResolution)? xResolution-1: x+radius;
	    int endPosY = (y+radius>=yResolution)? yResolution-1: y+radius;

	    float xCenter = (float) (x + 1./2);
	    float yCenter = (float) (y + 1./2);
	    
	    for(int i=startPosX; i<=endPosX; i++){
	        for(int j=startPosY; j<=endPosY; j++){
	        	if(Math.sqrt(Math.pow(xCenter-(i+1./2), 2) + Math.pow(yCenter-(j+1./2), 2)) <= radius) {
	        		result.add(this.map[i][j]);
	        	}
	        }
	    }
	    
	    return result;
	}
	
	public Collection<Cell> getCircleAround(Coordinate coord, int radius) {
		return this.getCircleAround(coord.x, coord.y, radius);
	}
	
	// Everything is placed by hand, to be randomized ...
	private void initializeAgents() {
		
		try {
			TownCenter townCenter = new TownCenter(
					new Coordinate(3*xResolution/4, 
								   (27*yResolution/50)+1), this);
			Map.townCenter = townCenter;
			this.addBuilding(townCenter);
		
			WaterTank waterTank = new WaterTank(
					new Coordinate(3*xResolution/4 + 8, 
								   (27*yResolution/50)-5), 1000, this);
			this.addBuilding(waterTank);
		
			Farm farm = new Farm(
					new Coordinate(3*xResolution/4 + 15, 
							  	   (27*yResolution/50)-12), 1000, this);
			this.addBuilding(farm);
		
			Kitchen kitchen = new Kitchen(
					new Coordinate(3*xResolution/4 + 4, 
								   (27*yResolution/50)+9), 3000, this);
			this.addBuilding(kitchen);
		
			AnimalStable animalStable = new AnimalStable(
					new Coordinate(3*xResolution/4 - 10, 
							       (27*yResolution/50)-2), 1000, this);
			this.addBuilding(animalStable);

			
			/* Agents */
			
			Human h1 = new Human(townCenter.coord, this);
			WaterCarrier waterCarrier1 = new WaterCarrier();
			// just to test ...
			waterCarrier1.setWaterTank(waterTank);
			waterCarrier1.setMissionTime(200);
			h1.setRole(waterCarrier1);
			this.addAgent(h1);
			
			Human h2 = new Human(townCenter.coord, this);
			h2.setRole(new Explorer());
			h2.setMissionTime(500);
			this.addAgent(h2);
			
			Human human3 = new Human(townCenter.coord, this);
			Cook cook = new Cook();
			cook.setWaterTank(waterTank);
			cook.setFoodShelter(farm);
			cook.setKitchen(kitchen);
			human3.setRole(cook);
			human3.setMissionTime(300);
			this.addAgent(human3);
			
			Human human5 = new Human(townCenter.coord, this);
			Cook cook2 = new Cook();
			cook2.setWaterTank(waterTank);
			cook2.setFoodShelter(animalStable);
			cook2.setKitchen(kitchen);
			human5.setRole(cook2);
			human5.setMissionTime(300);
			this.addAgent(human5);
			
			Human human4 = new Human(townCenter.coord, this);
			AnimalFarmer animalFarmer = new AnimalFarmer();
			animalFarmer.setAnimalStable(animalStable);
			human4.setRole(animalFarmer);
			human4.setMissionTime(300);
			this.addAgent(human4);
			
			Human h3 = new Human(farm.coord, this);
			Farmer farmer1 = new Farmer();
			farmer1.setFoodShelter(farm);
			h3.setRole(farmer1);
			h3.setMissionTime(300);
			this.addAgent(h3);
			
			/**
			 * TEST FARMER
			 */
			/*
			Farmer farmer1 = new Farmer();
			farmer1.setFoodShelter((FoodShelter) map[180][250].getBuilding());
			
			
			Human h3 = new Human(new Coordinate(320, 270), this);
			h3.setRole(farmer1);
			h3.setMissionTime(300);
			this.addAgent(h3);
			
			
			Farmer farmer2 = new Farmer();
			farmer2.setFoodShelter((FoodShelter) map[180][250].getBuilding());
			
			Human h4 = new Human(new Coordinate(320, 250), this);
			h4.setRole(farmer2);
			h4.setMissionTime(300);
			this.addAgent(h4);
			
			Farmer farmer3 = new Farmer();
			farmer3.setFoodShelter((FoodShelter) map[180][250].getBuilding());
			
			Human h5 = new Human(new Coordinate(320, 240), this);
			h5.setRole(farmer3);
			h5.setMissionTime(300);
			this.addAgent(h5);/*
			
			Farmer farmer4 = new Farmer();
			farmer4.setFoodShelter((FoodShelter) map[180][250].getBuilding());
			
			
			Human h7 = new Human(new Coordinate(320, 230), this);
			h7.setRole(farmer4);
			h7.setMissionTime(300);
			this.addAgent(h7);*/
			
			
			/**
			 * Hunter test
			 */
			Hunter hunter1 = new Hunter();
			hunter1.setFarm(farm);
			hunter1.setAnimalStable(animalStable);
			
			Human h9 = new Human(townCenter.coord, this);
			h9.setRole(hunter1);
			h9.setMissionTime(300);
			this.addAgent(h9);
			
			Hunter hunter2 = new Hunter();
			hunter2.setFarm(farm);
			hunter2.setAnimalStable(animalStable);
			
			Human h10 = new Human(townCenter.coord, this);
			h10.setRole(hunter2);
			h10.setMissionTime(300);
			this.addAgent(h10);
			
			
			this.initializeAnimal();
			
			
		} catch (NonConstructibleException e) {
			e.printStackTrace();
		}
	}

	private void initializeAnimal() {
		double r1;
		for(int i=0; i<xResolution; i++) {
			for(int j=0; j<yResolution; j++) {
				r1 = Math.random();
				if(r1<0.0007) {
					if(map[i][j].content == Content.forest) {
						this.addAgent(new Wolf(new Coordinate(i, j), this));
					} else {
						this.addAgent(new Sheep(new Coordinate(i,j), this));
					}
				}
			}
		}
	}
	
	private void generateForests() {
		for(int i=0; i<xResolution; i++) {
			for(int j=0; j<xResolution; j++) {
				
				double t = (float) NoiseWrapper.noise(i, j, 0, 
							                          5, 0.005, 1,
													  -0.8, 0.1, 0.9, 0.5);
				
				t = Math.abs(t);
				t = Math.min(t, 1);
				
				if(t<0.5 && map[i][j].topography != Topography.water) {
					map[i][j].content = Content.forest;
				} else {
					map[i][j].content = Content.empty;
				}
			}
		}
	}
	
	private void initializeMap() {
		map = new Cell[xResolution][yResolution];
		for(int i=0; i<xResolution; i++) {
			for(int j=0; j<xResolution; j++) {
				map[i][j] = new Cell(new Coordinate(i, j));
			}
		}
	}
	
	public boolean isVisitable(Coordinate coord) {
		return map[coord.x][coord.y].isVisitable();
	}
	
	public boolean isVisitableInForest(Coordinate coord) {
		return map[coord.x][coord.y].isVisitableInForest();
	}
	
	public void generateTopography() {
	
		for(int i=0; i<xResolution; i++) {
			for(int j=0; j<xResolution; j++) {
				
				double t = (float) NoiseWrapper.noise(i, j, 0, 
													  10, 0.01, 0.6,
													  0.8, -0.4, 0.9, 0.7);
								
				t = t*-7*Math.pow(i-xResolution/2, 2)/(xResolution*xResolution) +
						t*-7*Math.pow(j-yResolution/2, 2)/(yResolution*yResolution);
				t = Math.abs(t);
				t = Math.min(t, 1);
				t = 1-t;
				
				if(t<0.3) {
					map[i][j].topography = Topography.sea;
				}
			}
		}
		
		for(int i=0; i<xResolution; i++) {
			for(int j=0; j<xResolution; j++) {
//				bi.setRGB(i, j, (new Color(255, 0, 0)).getRGB());
				if(map[i][j].topography != Topography.sea) {
					double t = (float) NoiseWrapper.noise(i, j, 0, 
														  10, 0.01, 0.6,
														  0.8, -0.4, 0.9, 0.7);
								
//				t = t*-5*Math.pow(i-xResolution/2, 2)/(xResolution*xResolution) +
//						t*-5*Math.pow(j-yResolution/2, 2)/(yResolution*yResolution);
				t = Math.abs(t);
				t = Math.min(t, 1);
//				t = 1-t;
				
					if(t<0.4) {
						map[i][j].topography = Topography.water;
					} else if(t<0.85) {
						map[i][j].topography = Topography.plain;
					} else {
						map[i][j].topography = Topography.mountain;
					}
					
				}
			}
		}
	}
	
	
	private void waterToSea() {
		Set<Cell> checked = new HashSet<Cell>();
		Set<Cell> toCheck = new HashSet<Cell>();
		Boolean perform = true;
		toCheck.add(map[0][0]);
		map[0][0].topography = Topography.sea;
		Collection<Cell> around;
		while(perform) {
			Set<Cell> tmpToCheck = new HashSet<Cell>();
			Set<Cell> toCheckRemove = new HashSet<Cell>();
			for(Cell cell : toCheck) {
				toCheckRemove.add(cell);
				checked.add(cell);
				if(cell.topography == Topography.sea || cell.topography == Topography.water) {
					cell.topography = Topography.sea;
					around = this.getSquareAround(cell.coord, 1);
					for(Cell c : around) {
						if(!checked.contains(c)) {
							tmpToCheck.add(c);
						}
					}
				}
			}
			toCheck.removeAll(toCheckRemove);
			toCheck.addAll(tmpToCheck);
			perform = !toCheck.isEmpty();
		}
	}
	
	
	public BufferedImage toBufferedImage() {
		BufferedImage bi = new BufferedImage(xResolution, yResolution, 5);
		for(int i=0; i<xResolution; i++) {
			for(int j=0; j<xResolution; j++) {
				if(map[i][j].topography == Topography.water) {
					bi.setRGB(i, j, (new Color(0, 0, 255)).getRGB());
				} else if(map[i][j].topography == Topography.sea) {
					bi.setRGB(i, j, (new Color(0, 0, 200)).getRGB());
				} else if(map[i][j].topography == Topography.plain) {
					if(map[i][j].content == Content.forest) {
						bi.setRGB(i, j, (new Color(30, 75, 30)).getRGB());
					} else {
						bi.setRGB(i, j, (new Color(150, 255, 150)).getRGB());
					}
				} else if(map[i][j].topography == Topography.mountain) {
					if(map[i][j].content == Content.forest) {
						bi.setRGB(i, j, (new Color(10, 50, 10)).getRGB());
					} else {
						bi.setRGB(i, j, (new Color(255, 150, 50)).getRGB());
					}
				}
				
				if(! map[i][j].getAgents().isEmpty()) {
					bi.setRGB(i, j, ((Agent) map[i][j].getAgents().toArray()[0]).getColor().getRGB());
				}
				
				/**
				 * field color
				 */
				
				if(map[i][j].content == Content.building &&  map[i][j].getBuilding().getClass().toString().equals("class buildings.Field")) {
					
					bi.setRGB(i, j, (new Color(255, 215, 0)).getRGB());
				}
			}
		}
		return bi;
	}

	public void addAgent(MovingAgent agent) {
		if(map[agent.coord.x][agent.coord.y].isVisitable()) {
			map[agent.coord.x][agent.coord.y].addAgent(agent);
			this.agents.add(agent);
			this.allAgents.add(agent);
		}
	}
	
	public void removeAgent(Agent agent) {
		map[agent.coord.x][agent.coord.y].removeAgent(agent);
		this.agents.remove (agent);
		this.allAgents.remove(agent);
	}
	
	public Cell getCell(Coordinate c) {
		return this.getCell(c.x, c.y);
	}
	
	public Cell getCell(int x, int y) {
		return this.map[x][y];
	}

	public Collection<Agent> getAgents() {
		return this.agents;
	}
	
	public List<Agent> getAllAgents() {
		return this.allAgents;
	}
	
	/**
	 * @return the xResolution
	 */
	public int getXResolution() {
		return xResolution;
	}

	/**
	 * @return the yResolution
	 */
	public int getYResolution() {
		return yResolution;
	}
	
}
